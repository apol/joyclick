#include "joyclick.h"
#include <QGamepad>
#include <QLoggingCategory>

#include <KWayland/Client/connection_thread.h>
#include <KWayland/Client/fakeinput.h>
#include <KWayland/Client/registry.h>

joyclick::joyclick(QGamepad* pad)
    : QObject(pad)
{
    QLoggingCategory::setFilterRules(QStringLiteral("qt.gamepad.debug=true"));

    connect(pad, &QGamepad::axisLeftXChanged, this, [](double value){
        qDebug() << "Left X" << value;
    });
    connect(pad, &QGamepad::axisLeftYChanged, this, [](double value){
        qDebug() << "Left Y" << value;
    });
    connect(pad, &QGamepad::axisRightXChanged, this, [](double value){
        qDebug() << "Right X" << value;
    });
    connect(pad, &QGamepad::axisRightYChanged, this, [](double value){
        qDebug() << "Right Y" << value;
    });
    connect(pad, &QGamepad::buttonAChanged, this, [](bool pressed){
        qDebug() << "Button A" << pressed;
    });
    connect(pad, &QGamepad::buttonYChanged, this, [](bool pressed){
        qDebug() << "Button Y" << pressed;
    });
    connect(pad, &QGamepad::buttonL1Changed, this, [](bool pressed){
        qDebug() << "Button L1" << pressed;
    });
    connect(pad, &QGamepad::buttonR1Changed, this, [](bool pressed){
        qDebug() << "Button R1" << pressed;
    });
    connect(pad, &QGamepad::buttonL2Changed, this, [](double value){
        qDebug() << "Button L2: " << value;
    });
    connect(pad, &QGamepad::buttonR2Changed, this, [](double value){
        qDebug() << "Button R2: " << value;
    });
    connect(pad, &QGamepad::buttonSelectChanged, this, [](bool pressed){
        qDebug() << "Button Select" << pressed;
    });
    connect(pad, &QGamepad::buttonStartChanged, this, [](bool pressed){
        qDebug() << "Button Start" << pressed;
    });
    connect(pad, &QGamepad::buttonGuideChanged, this, [](bool pressed){
        qDebug() << "Button Guide" << pressed;
    });

    {
        using namespace KWayland::Client;
        ConnectionThread* connection = ConnectionThread::fromApplication(this);
        if (!connection) {
            qDebug() << "failed to get the Connection from Qt, Wayland remote input will not work";
            return;
        }
        Registry* registry = new Registry(this);
        registry->create(connection);
        connect(registry, &Registry::fakeInputAnnounced, this,
            [this, registry] (quint32 name, quint32 version) {
                m_waylandInput = registry->createFakeInput(name, version, this);
                connect(registry, &Registry::fakeInputRemoved, m_waylandInput, &QObject::deleteLater);
                m_waylandInput->authenticate({}, {});
            }
        );
        registry->setup();
    }

    connect(pad, &QGamepad::buttonBChanged, this, [this](bool pressed){
        if (!pressed && m_waylandInput) {
            qDebug() << "Button B" << pressed << m_waylandInput;
            m_waylandInput->requestPointerAxis(Qt::Vertical, 10);
        }
    });
    connect(pad, &QGamepad::buttonXChanged, this, [this](bool pressed){
        if (!pressed && m_waylandInput) {
            qDebug() << "Button X" << pressed << m_waylandInput;
            m_waylandInput->requestPointerAxis(Qt::Vertical, -10);
        }
    });
}

joyclick::~joyclick() = default;
