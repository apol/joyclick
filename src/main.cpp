#include "joyclick.h"
#include <QGuiApplication>
#include <QDebug>
#include <QGamepadManager>
#include <QGamepad>

int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);

    auto gamepads = QGamepadManager::instance()->connectedGamepads();
    if (gamepads.isEmpty()) {
        qDebug() << "Did not find any connected gamepads";
        return 1;
    }

    joyclick w(new QGamepad(*gamepads.begin(), &app));

    return app.exec();
}

