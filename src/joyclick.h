#ifndef JOYCLICK_H
#define JOYCLICK_H

#include <QObject>

class QGamepad;

namespace KWayland {
    namespace Client {
        class FakeInput;
    }
}

class joyclick : public QObject
{
    Q_OBJECT

public:
    explicit joyclick(QGamepad* pad);
    ~joyclick() override;


private:
    KWayland::Client::FakeInput* m_waylandInput = nullptr;
};

#endif // JOYCLICK_H
